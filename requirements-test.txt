pytest
pytest-mock
requests_mock
urllib3 < 2.0.0  # https://github.com/jamielennox/requests-mock/issues/228
swh-core[testing]
swh-scheduler[testing] >= 0.5.0
swh-storage[testing] >= 1.8.0
types-click
types-python-dateutil
types-pyyaml
types-requests
